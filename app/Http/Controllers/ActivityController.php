<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Activity;
use Auth;
use Session;

class ActivityController extends Controller
{
    public function index(){
    	$activities = Activity::all();
    	return view('bulletin', compact('activities'));
    }

    public function createActivity(){
    	$activities = Activity::all();

    	return view('adminviews.addactivity', compact('activities'));
    }

    public function storeActivity(Request $req){
    	$rules = array(
    		"title" => "required",
    		"description" => "required",
    		"date" => "required"
    	);

    	$this->validate($req, $rules);

    	// capture
    	$activity = new Activity;
    	$activity->title = $req->title;
    	$activity->description = $req->description;
    	$activity->date = $req->date;
    	$activity->save();

    	Session::flash("message", "$activity->title has been added");
    	return redirect()->back();
    }

    

    

    public function editActivity($id){
    	$activity = Activity::find($id);    	

    	return view('adminviews.editactivity', compact('activity'));
    }

    public function updateActivity($id, Request $req){
        $activity = Activity::find($id);

        $rules = array(
            "title" => "required",
            "description" => "required",
            "date" => "required"
        );

        $this->validate($req, $rules);

        $activity->title = $req->title;
        $activity->description = $req->description;
        $activity->date = $req->date;        

        $activity->save();
        Session::flash('message', "$activity->name has been updated");
        return redirect('/bulletin');
    }

    public function activityattendees(){
        $activities = Activity::all();

    	return view('userviews.activityattendees', compact('activities'));
    }
}
