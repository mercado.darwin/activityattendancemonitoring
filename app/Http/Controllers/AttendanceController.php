<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Activity;
use \App\Attendance;
use Auth;

class AttendanceController extends Controller
{
	public function createAttendance(){
    	$activities = Activity::all();

    	return view('userviews.createattendance', compact('activities'));
    }

    public function storeAttendance($id, Request $req){
    	$activity = Activity::find($id);
    	$attendances = Attendance::find(Auth::user()->id);
    	$activity->attendances()->attach($attendances);
    	$activity->save();
    	$order->save();

    	return view('userviews.createattendance', compact('activities'));
    }
}
