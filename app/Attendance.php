<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    public function activities(){
        return $this->belongsToMany("\App\Activity");
    }
}
