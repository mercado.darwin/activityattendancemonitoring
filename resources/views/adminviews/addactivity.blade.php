@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Add Activity Form</h1>

<div class="col-lg-4 offset-lg-4">
	<form action="/addactivity" method="POST" enctype="multipart/form-data">
		@csrf
		<div class="form-group">
			<label for="title">Title</label>
			<input type="text" name="title" class="form-control">
		</div>
		<div class="form-group">
			<label for="description">Description</label>
			<input type="text" name="description" class="form-control">
		</div>
		<div class="form-group">
			<label for="date">Date</label>
			<input type="text" name="date" class="form-control">
		</div>
		<button class="btn btn-warning" type="submit">Add Activity</button>
	</form>
</div>
@endsection