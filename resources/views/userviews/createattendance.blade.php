@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Create Attendance</h1>

<div class="col-lg-4 offset-lg-4">
	@foreach($activities as $activity)
	<form action="/createattendance/{{$activity->id}}" method="POST" enctype="multipart/form-data">
		@csrf
		<div class="form-group">
			<label for="activity_id">Activity</label>
			<select name="activity_id" class="form-control">
				
				<option value="{{$activity->id}}">{{$activity->title}}</option>
				
			</select>
		</div>
	@endforeach
		<button class="btn btn-success" type="submit">Submit Attendance</button>
	</form>

</div>
@endsection