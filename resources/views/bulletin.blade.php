@extends("layouts.app")
@section('content')

<h1 class="text-center py-5">Bulletin Board</h1>
<div class="container">
	<div class="row">
		<div class="col-lg-10">
			<div class="row w-100">
				@foreach($activities as $activity)
					<div class="col-lg-4 p-3 my-2">
						<div class="card">
							<div class="card-body">
								<h2 class="card-title">{{$activity->title}}</h2>
								<p class="card-text">{{$activity->description}}</p>
								<p class="card-text">{{$activity->date}}</p>
							</div>
							<a href="/editactivity/{{$activity->id}}" class="btn btn-success">Edit</a>
							<a href="/deleteactivity/{{$activity->id}}" class="btn btn-warning">Delete</a>
						</div>
					</div>

				@endforeach
			</div>
		</div>
	</div>
</div>

@endsection